package com.rc.rachelle.facereplacedemo;

import java.io.OutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.FrameLayout;
import android.view.*;
import android.view.View.MeasureSpec;
import android.util.Log;
import android.util.FloatMath;

public class InsertPersonActivity extends Activity
{
	private static final String TAG = "InsertPersonActivity";
	private static final String IMAGE_MANIPULATION_FRAGMENT = "ImageManipulationFragment";
	
	private RelativeLayout mRootRelativeLayout;
	private RelativeLayout mSurfaceContainer;
	private RelativeLayout mButtonContainer;
	private InsertView mInsertView;
	private ImageButton mNextButton;
	private Context mContext;
	private BitmapDrawable mBackground;
	
	private FragmentManager mFragmentManager;
 	private FragmentTransaction mFragmentTransaction;
	private ImageManipulationFragment mImageManipulationFragment;
	
	private FrameLayout mBottomFragmentContainer;
	private Resources mResources;

	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.insert_person_activity);
		
		mRootRelativeLayout = (RelativeLayout) findViewById(R.id.root_linearlayout_insertperson);
		mSurfaceContainer = (RelativeLayout) findViewById(R.id.surface_here);
		mButtonContainer = (RelativeLayout) findViewById(R.id.next_button_container);
		
		mFragmentManager = getFragmentManager();
		
		mBottomFragmentContainer = (FrameLayout) findViewById(R.id.bottom_fragment_container);
		
		mResources = getResources();
		
		if (SelfieSurpriseMainActivity.getBackgroundBMP() == null){
			Log.d(TAG, "Call to getBackgroundBMP returned null!");
		}
		
		mBackground = new BitmapDrawable(SelfieSurpriseMainActivity.getBackgroundBMP());
		mRootRelativeLayout.setBackgroundDrawable(mBackground);
		
		Intent intent = getIntent();
		//setting the default res id to R.drawable.prowlingkitty
		int resId = intent.getIntExtra("DRAWABLE_PERSON_ID", R.drawable.prowlingkitty);
		
		mContext = getApplicationContext();
		
		mNextButton = (ImageButton) findViewById(R.id.next_button);
		
		int width = (int) mBackground.getBitmap().getWidth()*2;
		int height = (int) (mBackground.getBitmap().getHeight())*3;
		mInsertView = new InsertView(mContext, width, height, resId);
		mInsertView.initialize();
		mSurfaceContainer.addView(mInsertView);
		
		mNextButton.setOnClickListener(new ImageButton.OnClickListener() {  
		        public void onClick(View v)
		            {
						Bitmap savedPic = prepareSurfaceCapture();
						SelfieSurpriseMainActivity.saveNewDrawing(savedPic);
						mSurfaceContainer.removeView(mInsertView);
						BitmapDrawable background = new BitmapDrawable(SelfieSurpriseMainActivity.getBackgroundBMP());
						mRootRelativeLayout.setBackgroundDrawable(background);
						//mInsertView.releaseZOrder();
						//mInsertView.removeGuidepoints();
						Uri savedPicUri = Utils.getImageUri(getApplicationContext(), savedPic);
						//mBottomFragmentContainer.bringToFront();
						mRootRelativeLayout.invalidate();
						//mInsertView.invalidate();
						launchManipulationFragment();
		            }
		         });
	}
	
	@Override
	public void onBackPressed(){
		launchManipulationFragment();
	}
	
	private void launchManipulationFragment(){	
	   mImageManipulationFragment = ImageManipulationFragment.getInstance(this);
	   FragmentTransaction ft2 = mFragmentManager.beginTransaction();
	   RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getBottomTaskBarHeight());
	   params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
	   mBottomFragmentContainer.setLayoutParams(params);
	   mBottomFragmentContainer.setVisibility(View.VISIBLE);
	   ft2.add(R.id.bottom_fragment_container, mImageManipulationFragment);
	   ft2.addToBackStack(IMAGE_MANIPULATION_FRAGMENT);
	   ft2.commit();
	}
	
	private int getBottomTaskBarHeight(){
		//Stays the same whether or not a fragment is inflated within the view
		int bottomTaskbarHeight = (int) mResources.getDimension(R.dimen.image_manipulation_frag_height);
		return bottomTaskbarHeight;
	}
	
	private Bitmap prepareSurfaceCapture(){
		//Two bitmaps and a final one to put them togther in
		Bitmap background = getBackgroundBitmap();
		Bitmap imposedBMP = mInsertView.getAddedBMP();
		Bitmap finalBitmap = Bitmap.createBitmap(background.getWidth(), background.getHeight(), background.getConfig());
		
		Canvas canvas = new Canvas(finalBitmap);
		canvas.drawBitmap(background, new Matrix(), null);
		
		float xPosition = mInsertView.getImageXPosition();
		float yPosition = mInsertView.getImageYPosition();
		Matrix imposedBMPMatrix = new Matrix();
		imposedBMPMatrix.postTranslate(xPosition, yPosition);
		//imposedBMPMatrix.postScale(2.0f, 2.0f);
		canvas.drawBitmap(imposedBMP, imposedBMPMatrix, null);
		
		return finalBitmap;
	}

	private Bitmap getBackgroundBitmap(){
		//make nextbutton invisible
		mNextButton.setVisibility(View.INVISIBLE);
		
		View view = mRootRelativeLayout;
		view.setDrawingCacheEnabled(true);
		view.buildDrawingCache(true);
		Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
		return bitmap;
	}
	
	
	public class InsertView extends SurfaceView implements SurfaceHolder.Callback {
		private static final String TAG = "InsertView";
		private Context mContext;
		private Bitmap mAddedBitmap;
		private Bitmap mGrabBitmap;
		private Canvas mGrabCanvas;
	    private int x=20,y=20;int width,height;
	    private int personToInsertResId;
		private ScaleGestureDetector mScaleDetector;
		private float mScaleFactor = 1.f;
		private float mLastBMPWidth, mLastBMPHeight;
		private float mInitialBMPWidth, mInitialBMPHeight;
		private Bitmap[] mSmallerResizedBitmaps, mBiggerResizedBitmaps;
		private int mLastXPosition, mLastYPosition;
		private Paint mCirclePaint;
		private float upperLeftX, upperLeftY, distance;
		private boolean keepingGuidepoints;
		
		public InsertView(Context context){
			super(context);
		}

	    public InsertView(Context context, int w, int h, int resId) {
	        super(context);
	        width=w;
	        height=h;
			personToInsertResId = resId;
			mContext = context;
	    }

		public void initialize(){
			getHolder().addCallback(this);
	        setFocusable(true);
			
			mAddedBitmap = BitmapFactory.decodeResource(getResources(), personToInsertResId);
			
			this.setDrawingCacheEnabled(true);
			
			mLastBMPWidth = mAddedBitmap.getWidth(); //mBMPWidth and Height get modified
			mLastBMPHeight = mAddedBitmap.getHeight();
			
			mInitialBMPWidth = mLastBMPWidth; //these do not get modified, they're for reference throughout the scaling process
			mInitialBMPHeight = mLastBMPHeight;
			
			mSmallerResizedBitmaps = new Bitmap[10];
			mBiggerResizedBitmaps = new Bitmap[10];

			populateSmallerBMPArray();
	
			this.setZOrderOnTop(true);    // necessary
			SurfaceHolder sfhTrackHolder = this.getHolder();
			sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);
			
			mCirclePaint = new Paint();
			mCirclePaint.setColor(Color.WHITE);
			mCirclePaint.setStrokeWidth(5.0f);
			
			keepingGuidepoints = true;
			
			mScaleDetector = new ScaleGestureDetector(mContext, new ScaleListener());	
		}

	   @Override
		    protected void onDraw(Canvas canvas) {
		        super.onDraw(canvas);
				upperLeftX = x-(mAddedBitmap.getWidth()/2);
				upperLeftY = y-(mAddedBitmap.getHeight()/2);
				distance = 150 * mScaleFactor;

				if (keepingGuidepoints){
			        canvas.drawBitmap(mAddedBitmap, upperLeftX, upperLeftY, null);
					canvas.drawCircle(x, y, 10.0f, mCirclePaint);
					canvas.drawCircle(x, y+distance, 10.0f, mCirclePaint);
				}
				else{
					canvas.drawBitmap(mAddedBitmap, upperLeftX, upperLeftY, null);
				}
				//canvas.drawCircle(upperLeftX+mAddedBitmap.getWidth(), upperLeftY, 10.0f, mCirclePaint); //right circle
				//canvas.drawCircle(upperLeftX, upperLeftY+mAddedBitmap.getHeight(), 10.0f, mCirclePaint); //left circle
		    }


	    @Override
	    public boolean onTouchEvent(MotionEvent event) {
			mScaleDetector.onTouchEvent(event); //hand the event over to mScaleDetector
			final int action = event.getAction();
			switch(action){
				case MotionEvent.ACTION_MOVE: { //single-finger movement
			        x=(int)event.getX();
			        y=(int)event.getY();

			        if(x<25)
			            x=25;
			        if(x> width)   
			            x=width;
			        if(y <25)
			            y=25;
			        if(y > height)
			            y=height;

					//The movement and pinch-to-resize gesture are jumpy because as soon as a single pointer touches the screen, the picture
					//will move its center to that position. We should only let the user move the picture by dragging with their finger
					//within the bounds of the picture as it's on the screen
		
					if ((mLastXPosition < x) && (x < mLastXPosition+mAddedBitmap.getWidth()) && 
					(mLastYPosition < y) && (y < mLastYPosition+mAddedBitmap.getHeight())){
						mLastXPosition = x-(mAddedBitmap.getWidth()/2);
						mLastYPosition = y-(mAddedBitmap.getHeight()/2);
			        	updateView();
					}

			        break;
				}
			}
			return true;
	    }
	
		public void removeGuidepoints(){
			keepingGuidepoints = false;
			updateView();
		}

		public void releaseZOrder(){
			this.setZOrderOnTop(false);
		}
	
		public float getImageXPosition(){
			//I will get the current coordinates of the superimposed Bitmap and save it into place on the underlying background image
			return (float) mLastXPosition;
		}
		
		public float getImageYPosition(){
			return (float) mLastYPosition;
		}
		
		public Bitmap getAddedBMP(){
			return mAddedBitmap;
		}

	    @Override
	    public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
	        // TODO Auto-generated method stub
	    }

	    @Override
	    public void surfaceCreated(SurfaceHolder holder) {
	        // TODO Auto-generated method stub
	    }

	    @Override
	    public void surfaceDestroyed(SurfaceHolder holder) {
	        // TODO Auto-generated method stub
	    }

	    private void updateView() {
	        Canvas canvas = null;
	        try {
	            canvas = getHolder().lockCanvas(null);
	            synchronized (getHolder()) {
					canvas.drawColor( 0, PorterDuff.Mode.CLEAR );
	                this.onDraw(canvas);
	            }
	        }
	        finally {
	            if (canvas != null) {
	                getHolder().unlockCanvasAndPost(canvas);
	            }
	        }
	    }
	
		private void populateSmallerBMPArray(){
			int width, height;
			for (int i=0; i<10; i++){
				switch(i){
					case 0:
						width = (int)Math.round(mInitialBMPHeight*.9);
						height = (int)Math.round(mInitialBMPWidth*.9);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 1:
						width = (int)Math.round(mInitialBMPHeight*.8);
						height = (int)Math.round(mInitialBMPWidth*.8);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 2:
						width = (int)Math.round(mInitialBMPHeight*.7);
						height = (int)Math.round(mInitialBMPWidth*.7);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 3:
						width = (int)Math.round(mInitialBMPHeight*.6);
						height = (int)Math.round(mInitialBMPWidth*.6);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 4:
						width = (int)Math.round(mInitialBMPHeight*.5);
						height = (int)Math.round(mInitialBMPWidth*.5);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 5:
						width = (int)Math.round(mInitialBMPHeight*.4);
						height = (int)Math.round(mInitialBMPWidth*.4);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 6:
						width = (int)Math.round(mInitialBMPHeight*.3);
						height = (int)Math.round(mInitialBMPWidth*.3);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 7:
						width = (int)Math.round(mInitialBMPHeight*.2);
						height = (int)Math.round(mInitialBMPWidth*.2);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 8:
						width = (int)Math.round(mInitialBMPHeight*.1);
						height = (int)Math.round(mInitialBMPWidth*.1);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
					case 9:
						width = (int)Math.round(mInitialBMPHeight*.05);
						height = (int)Math.round(mInitialBMPWidth*.05);
						mSmallerResizedBitmaps[i] = Utils.getResizedBitmap(mAddedBitmap, width, height);
						break;
				}
			}
		}
	
		private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

			private int mNewBMPWidth, mNewBMPHeight;
			private Bitmap mClosestScaleBitmap;

		    @Override
		    public boolean onScale(ScaleGestureDetector detector) {
			    mScaleFactor *= detector.getScaleFactor();

			    // Don't let the object get too small or too large.
			    mScaleFactor = Math.max(0.1f, Math.min(mScaleFactor, 5.f));

				mNewBMPWidth = Math.abs(Math.round(mLastBMPWidth*mScaleFactor));
				mNewBMPHeight = Math.abs(Math.round(mLastBMPHeight*mScaleFactor));
			
				//Checking that the image is not too huge or tiny resolves the out of memory
				//issues, but not the pixelation
				if ((mNewBMPWidth < 50) || (mNewBMPHeight < 50)){
					return false;
				}
			
				if ((mNewBMPWidth > 600) || (mNewBMPHeight > 600)){
					return false;
				}

				mLastBMPWidth = mNewBMPWidth;
				mLastBMPHeight = mNewBMPHeight;

				float ratio = mNewBMPWidth/mInitialBMPWidth; //doesn't matter if we use width or height, 
				mClosestScaleBitmap = getScaledBitmapFromArrays(ratio);
				mAddedBitmap = Utils.scaleBitmap(mClosestScaleBitmap, mNewBMPWidth, mNewBMPHeight); 
				//this makes it so that we scale from a saved copy which is like mAddedBitmap's closest 
				//scale neighbor at the given size -- this helps a ton with the pixelation issues, almost entirely
				//resolving them unless the image is very tiny

			    invalidate();
	
				return true;
		    }
		
			public Bitmap getScaledBitmapFromArrays(float ratio){
				if (ratio == 1){
					return mAddedBitmap;
				}
				//if smaller
				else if (1>ratio && ratio >= 0.9){
					return mSmallerResizedBitmaps[0]; //nearest neighbor is the scaled BMP at 0.9
				}
				else if (0.9 > ratio && ratio >= 0.8 ){
					return mSmallerResizedBitmaps[1];
				}
				else if (0.8 > ratio && ratio >= 0.7){
					return mSmallerResizedBitmaps[2];
				}
				else if (0.7 > ratio && ratio >= 0.6){
					return mSmallerResizedBitmaps[3];
				}
				else if (0.6 > ratio && ratio >= 0.5){
					return mSmallerResizedBitmaps[4];
				}
				else if (0.5 > ratio && ratio >= 0.4){
					return mSmallerResizedBitmaps[5];
				}
				else if (0.4 > ratio && ratio >= 0.3){
					return mSmallerResizedBitmaps[6];
				}
				else if (0.3 > ratio && ratio >= 0.2){
					return mSmallerResizedBitmaps[7];
				}
				else if (0.2 > ratio && ratio >= 0.1){
					return mSmallerResizedBitmaps[8];
				}
				else {
				return mSmallerResizedBitmaps[0];
				}	
			}
		}

	}
	
}