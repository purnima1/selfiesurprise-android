package com.rc.rachelle.facereplacedemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

public class FaceReplaceDemoMainActivity extends Activity
{
	  /*
			TODO: Add button to choose picture from Dropbox/SDCard
	  */
    
	private Button takePictureButton;
	private Button choosePictureButton;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

		takePictureButton = (Button) findViewById(R.id.main_take_picture_button);
		takePictureButton.setOnClickListener(new Button.OnClickListener() {
		        public void onClick(View v)
		            {
		            	Intent start = new Intent(FaceReplaceDemoMainActivity.this, TakePictureActivity.class);
						startActivity(start);
		            }
		         });
		
		choosePictureButton = (Button) findViewById(R.id.main_choose_picture_button);
		choosePictureButton.setOnClickListener(new Button.OnClickListener(){
				public void onClick(View v)
		            {
		            	Intent start = new Intent(FaceReplaceDemoMainActivity.this, ChoosePictureActivity.class);
						startActivity(start);
		            }
		         });
    }
}

