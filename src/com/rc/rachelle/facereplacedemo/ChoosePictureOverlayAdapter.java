package com.rc.rachelle.facereplacedemo;

import java.util.List;
import java.util.ArrayList;

import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.ImageView;
import android.widget.GridView;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.view.ViewGroup;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.content.res.Resources;

public class ChoosePictureOverlayAdapter extends BaseAdapter
{
	private Context mContext;
	private List<Bitmap> mLibraryPictures;
	private int mWidth, mHeight;
	private Bitmap mLibraryPicture;
	
	    public ChoosePictureOverlayAdapter(Context c, List<Bitmap> libraryPictures, int width, int height){
	      	this.mContext = c;
			this.mLibraryPictures = libraryPictures;
			this.mWidth = width;
			this.mHeight = height;
	    }

	    public int getCount() {
	        return mLibraryPictures.size();
	    }

	    public Bitmap getItem(int position) {
	        return mLibraryPictures.get(position);
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	 	// create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent){

			ImageView imageView;
			
			if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(mWidth, mHeight));
				//imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
				imageView.setPadding(5,5,5,5);
	        } else {
	            imageView = (ImageView) convertView;
	        }
		
			//get mLibraryPicture Drawable
			mLibraryPicture = mLibraryPictures.get(position);
			
			imageView.setImageBitmap(mLibraryPicture);

			return imageView;
		}
	
	
}
