package com.rc.rachelle.facereplacedemo;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.io.File;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.graphics.Paint.Style;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Matrix;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.graphics.Bitmap.CompressFormat;



public class Utils{
	
	private static final String TAG = "Utils: ";
	
	public static final String IMAGE_MANIPULATION_FRAGMENT = "imageManipFrag";
	public static final String FIRST_LOAD = "firstLoad";
	
	public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    bitmap.compress(CompressFormat.PNG, 0, outputStream);       
	    return outputStream.toByteArray();
	}
	
	public static Bitmap rotateBitmap(Bitmap source, float angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	
	public static Bitmap convert(Bitmap bitmap, Bitmap.Config config) {
		//Must convert the bitmap to RGB_565 in order for the FaceDetector tools to work
	    Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
	    Canvas canvas = new Canvas(convertedBitmap);
	    Paint paint = new Paint();
	    paint.setColor(Color.BLACK);
	    canvas.drawBitmap(bitmap, 0, 0, paint);
	    return convertedBitmap;
	}
	
	public static Bitmap scaleBitmapSelfie(Bitmap bitmapToScale, float newWidth, float newHeight) {   
		if(bitmapToScale == null)
		    return null;
		//get the original width and height
		float width = bitmapToScale.getWidth();
		float height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix, true);  
	}
	
	
	public static Bitmap addRectangularFrame(Bitmap bmp, int borderSize, int colorChoice){
	    Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize * 2, bmp.getHeight() + borderSize * 2, bmp.getConfig());
	    Canvas canvas = new Canvas(bmpWithBorder);
		if (colorChoice == 0){ canvas.drawColor(Color.BLACK); }
		else if (colorChoice == 1){ canvas.drawColor(Color.WHITE); }
		else if (colorChoice == 2){ canvas.drawColor(Color.TRANSPARENT); } //could use to add margins where the usual programmatic route is being finnicky
	    canvas.drawBitmap(bmp, borderSize, borderSize, null);
	    return bmpWithBorder;	
	}
	
	public static Bitmap toGrayscale(Bitmap bmpOriginal)
	{        
	    int width, height;
	    height = bmpOriginal.getHeight();
	    width = bmpOriginal.getWidth();    

	    Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
	    Canvas c = new Canvas(bmpGrayscale);
	    Paint paint = new Paint();
	    ColorMatrix cm = new ColorMatrix();
	    cm.setSaturation(0);

	    ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
	    paint.setColorFilter(f);

	    c.drawBitmap(bmpOriginal, 0, 0, paint);
	    return bmpGrayscale;
	}
	
	public static Bitmap toStrictBlackWhite(Bitmap bmp){
	        Bitmap imageOut = bmp;
	        int tempColorRed;
	        for(int y=0; y<bmp.getHeight(); y++){
	            for(int x=0; x<bmp.getWidth(); x++){
	                tempColorRed = Color.red(imageOut.getPixel(x,y));
	                Log.v(TAG, "COLOR: "+tempColorRed);

	                if(imageOut.getPixel(x,y) < 127){
	                    imageOut.setPixel(x, y, 0xffffff);
	                }
	                else{
	                    imageOut.setPixel(x, y, 0x000000);
	                }               
	            }
	        } 
	        return imageOut;
	    }
	
	public static Uri getImageContentUri(Context context, File imageFile) {
	        String filePath = imageFile.getAbsolutePath();
	        Cursor cursor = context.getContentResolver().query(
	                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
	                new String[] { MediaStore.Images.Media._ID },
	                MediaStore.Images.Media.DATA + "=? ",
	                new String[] { filePath }, null);
	        if (cursor != null && cursor.moveToFirst()) {
	            int id = cursor.getInt(cursor
	                    .getColumnIndex(MediaStore.MediaColumns._ID));
	            Uri baseUri = Uri.parse("content://media/external/images/media");
	            return Uri.withAppendedPath(baseUri, "" + id);
	        } else {
	            if (imageFile.exists()) {
	                ContentValues values = new ContentValues();
	                values.put(MediaStore.Images.Media.DATA, filePath);
	                return context.getContentResolver().insert(
	                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
	            } else {
	                return null;
	            }
	        }
	    }
	
	public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth){
		    int width = bm.getWidth();
		    int height = bm.getHeight();
		    float scaleWidth = ((float) newWidth) / width;
		    float scaleHeight = ((float) newHeight) / height;
		    // create a matrix for the manipulation
		    Matrix matrix = new Matrix();
		    // resize the bit map
		    matrix.postScale(scaleWidth, scaleHeight);
		    // recreate the new Bitmap
		    Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		    return resizedBitmap;
	}
	
	public static Bitmap scaleBitmap(Bitmap bitmap, int newWidth, int newHeight) {
	  Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Config.ARGB_8888);

	  float scaleX = newWidth / (float) bitmap.getWidth();
	  float scaleY = newHeight / (float) bitmap.getHeight();
	  float pivotX = 0;
	  float pivotY = 0;

	  Matrix scaleMatrix = new Matrix();
	  scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

	  Canvas canvas = new Canvas(scaledBitmap);
	  canvas.setMatrix(scaleMatrix);
	  canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

	  return scaledBitmap;
	}
	
	public static Uri getImageUri(Context inContext, Bitmap inImage) {
	  String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
	  return Uri.parse(path);
	}
	
	public static Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
		   // First decode with inJustDecodeBounds=true to check dimensions
		   final BitmapFactory.Options options = new BitmapFactory.Options();

		   // Calculate inSampleSize
		   //options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
		   options.inSampleSize = 4; //push this up from 4 to reduce sampling size
		
		   Bitmap bm = BitmapFactory.decodeFile(path, options); 
		
		   if (bm == null){
				return null;}
		
		   //if image is horizontal, we will need to rotate it
		   if (bm.getWidth() > bm.getHeight()){
				bm = rotateBitmap(bm, 90);
		   }

		   Log.d(TAG, "Decoded image path: "+path);

	   	   return bm;   
	  }
	
	 public static Bitmap mirrorBitmap(Bitmap bitmap){
		Matrix matrix = new Matrix(); 
		matrix.preScale(-1.0f, 1.0f); 
		Bitmap mirroredBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
		return mirroredBitmap;
	 }
	
	 public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight){
		   // Raw height and width of image
		   final int height = options.outHeight;
		   final int width = options.outWidth;
		   int inSampleSize = 8;

		   if (height > reqHeight || width > reqWidth) {
		    if (width > height) {
		     inSampleSize = Math.round((float)height / (float)reqHeight);    
		    } else {
		     inSampleSize = Math.round((float)width / (float)reqWidth);    
		    }   
		   }

		return inSampleSize;    
	  }
	
}