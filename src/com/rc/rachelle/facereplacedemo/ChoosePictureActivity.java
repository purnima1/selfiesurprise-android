package com.rc.rachelle.facereplacedemo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.util.Vector;

import com.polites.android.GestureImageView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Files.FileColumns;
import android.net.Uri;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.*;
import android.view.ViewGroup.LayoutParams;
import android.util.AttributeSet;
import android.util.Log;

public class ChoosePictureActivity extends Activity{
	
	private static String TAG = "ChoosePictureActivity";
	
	private RelativeLayout mRelativeLayout;
	private ImageView mBackgroundImage;
	private Bitmap mBackgroundBitmap;
	private Drawable mBackgroundDrawable;
	private BallView mBallView;
	private Button mMoveButton, mResizeButton, mRotateButton;
	private int w, h;
	private Context mContext;
	
	private static final int REQUEST_CODE = 1;
	private Uri selectedImageURI;

	@Override
    public void onCreate(Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choose_picture_activity);
		
		mRelativeLayout = (RelativeLayout) findViewById(R.id.insertingView);
		
		mContext = getApplicationContext();
		
		/*
		if (mBackgroundBitmap == null){
		   Intent intent = new Intent();
		   intent.setType("image/*");
		   intent.setAction(Intent.ACTION_GET_CONTENT);
		   intent.addCategory(Intent.CATEGORY_OPENABLE);
		   startActivityForResult(intent, REQUEST_CODE);
		}
		*/
		
		//Todo: change these to the width and height of the underlying bitmap
		w = getWindowManager().getDefaultDisplay().getWidth()-25;
	    h = getWindowManager().getDefaultDisplay().getHeight()-25;
		
		mBackgroundImage = new ImageView(this);
		//mBackgroundDrawable = getResources().getDrawable(R.drawable.rc_office);
		Bitmap background = BitmapFactory.decodeResource(getResources(),
		                                           R.drawable.rc_office);
	 	background = Bitmap.createScaledBitmap(background, w, h, false);
		background = Utils.rotateBitmap(background, 90);
		mBackgroundImage.setImageBitmap(background);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
		  LinearLayout.LayoutParams.MATCH_PARENT, 
		  LinearLayout.LayoutParams.MATCH_PARENT);
		mBackgroundImage.setLayoutParams(lp);
		mRelativeLayout.addView(mBackgroundImage);
		
		mMoveButton = (Button) findViewById(R.id.move_image);
		mMoveButton.setOnClickListener(new Button.OnClickListener() {
			    @Override
		        public void onClick(View v)
		            {
						mBallView = new BallView(mContext, w, h);
						mRelativeLayout.addView(mBallView);
		            }
		         });
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		InputStream inputStream = null;
		if (requestCode ==  REQUEST_CODE && resultCode == Activity.RESULT_OK){
			//User chose picture from library
			try{ //recycle unused bitmaps
				if (mBackgroundBitmap != null){ mBackgroundBitmap.recycle();}
				selectedImageURI = data.getData();
				Log.d(TAG,"Image URI being handed to getRealPath: "+selectedImageURI);
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inSampleSize = 4; //few things from SOF posters recommended to avoid out of memory errors
				o.inDither=false;                   
				o.inPurgeable=true;                   
				mBackgroundBitmap = BitmapFactory.decodeFile(getRealPathFromURI(selectedImageURI));
				Log.d(TAG,"Real Path computed: "+getRealPathFromURI(selectedImageURI));
				mBackgroundBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap, w, h, false);
				mBackgroundImage.setImageBitmap(mBackgroundBitmap);
				mRelativeLayout.addView(mBackgroundImage);
			}
			
			catch (Exception e){
				e.printStackTrace();}
			finally {
				if (inputStream != null){
					try{inputStream.close();}
					catch (IOException e){ e.printStackTrace();}	
				}	
			}	
		}
	}
	
	private String getRealPathFromURI(Uri contentURI) {
	    String result;
	    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		Log.d(TAG,"Content URI: "+contentURI);
	    if (cursor == null) { // Source is Dropbox or other similar local file path
	        result = contentURI.getPath();
			Log.d(TAG,"Image URI: "+result);
	    } else { 
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	        result = cursor.getString(idx);
			Log.d(TAG,"Image URI: "+result);
	        cursor.close();
	    }
	    return result;
	}
	
	public class BallView extends SurfaceView implements SurfaceHolder.Callback {
		private static final String TAG = "BallView";
		private Bitmap mAddedBitmap;
	    private int x=20,y=20;int width,height;

	    public BallView(Context context,int w,int h) {
	        super(context);

	        width=w;
	        height=h;
	        getHolder().addCallback(this);
	        setFocusable(true);
	
			mAddedBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.prowlingkitty);
	
			this.setZOrderOnTop(true);    // necessary
			SurfaceHolder sfhTrackHolder = this.getHolder();
			sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);
	    }

	    @Override
	    protected void onDraw(Canvas canvas) {
	        super.onDraw(canvas);
	        canvas.drawBitmap(mAddedBitmap,x-(mAddedBitmap.getWidth()/2),y-(mAddedBitmap.getHeight()/2),null);
	    }

	    @Override
	    public boolean onTouchEvent(MotionEvent event) {
	        x=(int)event.getX();
	        y=(int)event.getY();

	        if(x<25)
	            x=25;
	        if(x> width)   
	            x=width;
	        if(y <25)
	            y=25;
	        if(y > height)
	            y=height;

	        updateBall();

	        return true;
	    }

	    @Override
	    public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
	        // TODO Auto-generated method stub
	    }

	    @Override
	    public void surfaceCreated(SurfaceHolder holder) {
	        // TODO Auto-generated method stub
	    }

	    @Override
	    public void surfaceDestroyed(SurfaceHolder holder) {
	        // TODO Auto-generated method stub
	    }

	    private void updateBall() {
	        Canvas canvas = null;
	        try {
	            canvas = getHolder().lockCanvas(null);
	            synchronized (getHolder()) {
					canvas.drawColor( 0, PorterDuff.Mode.CLEAR );
	                this.onDraw(canvas);
	            }
	        }
	        finally {
	            if (canvas != null) {
	                getHolder().unlockCanvasAndPost(canvas);
	            }
	        }
	    }   
	}
	
}