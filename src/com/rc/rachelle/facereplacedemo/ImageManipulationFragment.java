package com.rc.rachelle.facereplacedemo;

import java.util.List;
import java.util.Arrays;
import java.util.Random;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.util.Log;

import com.rc.rachelle.facereplacedemo.R;

public class ImageManipulationFragment extends Fragment implements OnClickListener{
	
	private static final String TAG = "ImageManipulationFragment: ";
	
	private static final int REDO_PIC = 0;
	private static final int ADD_PERSON = 1;
	private static final int REPLACE_FACE = 2;
	private static final int ADD_CAPTION = 3;
	private static final int FINISH = 4;
	
	private LinearLayout mRootLinearLayout;
	private FrameLayout.LayoutParams mCompressedParams;
	private FrameLayout.LayoutParams mExpandedParams;
	private static Activity mMainInstance;
	private FragmentManager mFragmentManager;
	private View mView;
	private ImageButton mInsertPerson, mReplaceFace, mAddCaption, mRedo, mFinish;
	private ImageView mTopDivBar;
	private HorizontalScrollView mInsertPersonHSV, mReplaceFaceHSV;
	private ImageButton mRandomInsertPerson, mRandomReplaceFace;
	private int[] mInsertPersonChoices={R.id.borat_button, R.id.cat_2_button, R.id.dogs_button, R.id.giraffe_1_button, R.id.godzilla_button, R.id.peewee_button, R.id.superman_2_button, R.id.beetlejuice_button, R.id.cat_1_button, R.id.chewbacca_button, R.id.fat_bastard_button, R.id.giraffe_2_button, R.id.monkey_button, R.id.superman_button};
	private int[] mReplaceFaceChoices={R.id.alien_button, R.id.angelina_button, R.id.bond_button, R.id.brad_button, R.id.larry_button, R.id.curly_button, R.id.moe_button, R.id.obama_button, R.id.potter_button};

	private ImageButton[] mInsertPersonButtons, mReplaceFaceButtons;
	
	private boolean insertOrReplace; //false corresponds to insertPerson, true corresponds to replaceFace
	private int mPhotoOperation;
	
	public ImageManipulationFragment(){}

	public static ImageManipulationFragment getInstance(Activity callingActivity){
		//Take an argument of the SelfieSurpriseMainActivity; it's the only Activity that will ever be calling for this Fragment
		ImageManipulationFragment imageManip = new ImageManipulationFragment();
		mMainInstance = callingActivity;
		Log.d(TAG, "Created instance of ImageManipulationFragment from class: "+mMainInstance.getClass());
		return imageManip;
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		Log.d(TAG, "onAttach() called");
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		super.onCreateView(inflater, container, savedInstanceState);
		
		mView = inflater.inflate(R.layout.image_manipulation_fragment, container, false);
		
		mRootLinearLayout = (LinearLayout) mView.findViewById(R.id.root_linearlayout_imagemanip);
		mRandomInsertPerson = (ImageButton) mView.findViewById(R.id.random_button_insertperson);
		mRandomReplaceFace = (ImageButton) mView.findViewById(R.id.random_button_face);

		//We'll set these to switch between expanded or compressed heights for the fragment
		mCompressedParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, R.dimen.image_manipulation_frag_height_compressed);
		mExpandedParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, R.dimen.image_manipulation_frag_height_expanded);
		
		mRedo = (ImageButton) mView.findViewById(R.id.redo_picture_button);
		mInsertPerson = (ImageButton) mView.findViewById(R.id.insert_person_button);
		mReplaceFace = (ImageButton) mView.findViewById(R.id.replace_face_button);
		mAddCaption = (ImageButton) mView.findViewById(R.id.add_caption_button);
		mFinish = (ImageButton) mView.findViewById(R.id.finish_button);
		
		mTopDivBar = (ImageView) mView.findViewById(R.id.div_bar_top);
		
		mInsertPersonHSV = (HorizontalScrollView) mView.findViewById(R.id.add_person_scrollview);
		mReplaceFaceHSV = (HorizontalScrollView) mView.findViewById(R.id.replace_face_scrollview);
		//insertOrReplace = false; //insertPerson is visible by default
		mPhotoOperation = 0;
		
		mRedo.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            { 
					redoFromStart();
	            }
	         });
		
		mRandomReplaceFace.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            { 
					chooseRandomFaceReplacement();
	            }
	         });
		
		mRandomInsertPerson.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            { 
					chooseRandomInsertPerson();
	            }
	         });
		
		mInsertPerson.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            { //toggle visibilty of insert person scrollview
					mRootLinearLayout.setLayoutParams(mExpandedParams);
					mReplaceFaceHSV.setVisibility(View.GONE);
					mInsertPersonHSV.setVisibility(View.VISIBLE);
					mTopDivBar.setVisibility(View.VISIBLE);	
					mPhotoOperation = ADD_PERSON;
	            }
	         });
	
		mReplaceFace.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            { //toggle visibilty of replace person scrollview
					mRootLinearLayout.setLayoutParams(mExpandedParams);
					mInsertPersonHSV.setVisibility(View.GONE);
					mReplaceFaceHSV.setVisibility(View.VISIBLE);
					mTopDivBar.setVisibility(View.VISIBLE);	
					mPhotoOperation = REPLACE_FACE;
	            }
	         });
	
		mAddCaption.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            { //toggle visibilty of unrelated UI elements to GONE
					mInsertPersonHSV.setVisibility(View.GONE);
					mReplaceFaceHSV.setVisibility(View.GONE);
					mTopDivBar.setVisibility(View.GONE);
					mRootLinearLayout.setLayoutParams(mCompressedParams);	
					mPhotoOperation = ADD_CAPTION;
					
					Intent addCaption = new Intent(mMainInstance.getApplicationContext(), AddCaptionActivity.class);
					startActivity(addCaption);
	            }
	         });
	
		mFinish.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            { //toggle visibilty of unrelated UI elements to GONE
					mInsertPersonHSV.setVisibility(View.GONE);
					mReplaceFaceHSV.setVisibility(View.GONE);
					mTopDivBar.setVisibility(View.GONE);
					mRootLinearLayout.setLayoutParams(mCompressedParams);	
					mPhotoOperation = FINISH;

					Intent finish = new Intent(mMainInstance.getApplicationContext(), ShareActivity.class);
					startActivity(finish);
	            }
	         });
		
		setClickListenersInsertPersonArray();
		setClickListenersReplaceFaceArray();
		
		Log.d(TAG, "Created the View for ImageManipulationFragment");
		
		return mView;
	}
	
	private void redoFromStart(){
		SelfieSurpriseMainActivity.clearList();
		Intent restart = new Intent(mMainInstance.getApplicationContext(), SelfieSurpriseMainActivity.class);
		restart.putExtra(Utils.FIRST_LOAD, false);
		startActivity(restart);
	}
	
	public void setClickListenersInsertPersonArray(){
		//and also 
		mInsertPersonButtons = new ImageButton[mInsertPersonChoices.length];
		for (int i=0; i<mInsertPersonButtons.length; i++){
			mInsertPersonButtons[i] = ((ImageButton) mView.findViewById(mInsertPersonChoices[i]));
			mInsertPersonButtons[i].setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			mInsertPersonButtons[i].setOnClickListener(this);
		}
	}
	
	public void setClickListenersReplaceFaceArray(){
		mReplaceFaceButtons = new ImageButton[mReplaceFaceChoices.length];
		for (int i=0; i<mReplaceFaceButtons.length; i++){
			mReplaceFaceButtons[i] = ((ImageButton) mView.findViewById(mReplaceFaceChoices[i]));
			mReplaceFaceButtons[i].setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			mReplaceFaceButtons[i].setOnClickListener(this);
		}
	}
	
	private void chooseRandomFaceReplacement(){
		int indexReplacementButton = randInt(0, mReplaceFaceChoices.length-1);
		int personToReplaceButton = mReplaceFaceChoices[indexReplacementButton];
		int personToReplaceDrawable = getReplacementFace(personToReplaceButton);
		
		Intent replacePerson = new Intent(mMainInstance.getApplicationContext(), ReplaceFaceActivity.class);
		replacePerson.putExtra("FACE_ID", personToReplaceDrawable);
		startActivity(replacePerson);
	}
	
	public int getReplacementFace(int selectedButtonId){
		int drawableFace = 0;
		switch (selectedButtonId){
			case R.id.alien_button:
				drawableFace = R.drawable.alien_mask;
				break;
			case R.id.angelina_button:
				drawableFace = R.drawable.angelina_mask;
				break;
			case R.id.bond_button:
				drawableFace = R.drawable.bond_mask;
				break;
			case R.id.brad_button:
				drawableFace = R.drawable.brad_mask;
				break;
			case R.id.curly_button:
				drawableFace = R.drawable.curly_mask;
				break;
			case R.id.larry_button:
				drawableFace = R.drawable.larry_mask;
				break;
			case R.id.moe_button:
				drawableFace = R.drawable.moe_mask;
				break;
			case R.id.obama_button:
				drawableFace = R.drawable.obama_mask;
				break;
			case R.id.potter_button:
				drawableFace = R.drawable.potter_mask;
			default:
				break;
		}
		return drawableFace;
	}
	
	public void chooseRandomInsertPerson(){
		int indexInsertPerson = randInt(0, mInsertPersonChoices.length-1);
		int personToInsertButton = mInsertPersonChoices[indexInsertPerson];
		int personToInsertDrawable = getPersonToInsert(personToInsertButton);
		
		Intent insertPerson = new Intent(mMainInstance.getApplicationContext(), InsertPersonActivity.class);
		insertPerson.putExtra("DRAWABLE_PERSON_ID", personToInsertDrawable);
		startActivity(insertPerson);
	}
	
	private int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}
	
	public int getPersonToInsert(int selectedButtonId){
		
		int drawablePerson = 0;
		
		switch (selectedButtonId){
			case R.id.borat_button:
				drawablePerson = R.drawable.borat;
				break;
			case R.id.cat_2_button:
				drawablePerson = R.drawable.cat_2;
				break;
			case R.id.dogs_button:
				drawablePerson = R.drawable.dogs;
				break;
			case R.id.giraffe_1_button:
				drawablePerson = R.drawable.giraffe_1;
				break;
			case R.id.godzilla_button:
				drawablePerson = R.drawable.godzilla;
				break;
			case R.id.peewee_button:
				drawablePerson = R.drawable.peewee;
				break;
			case R.id.superman_2_button:
				drawablePerson = R.drawable.superman_2;
				break;
			case R.id.beetlejuice_button:
				drawablePerson = R.drawable.beetlejuice;
				break;
			case R.id.cat_1_button:
				drawablePerson = R.drawable.cat_1;
				break;
			case R.id.chewbacca_button:
				drawablePerson = R.drawable.chewbacca;
				break;
			case R.id.fat_bastard_button:
				drawablePerson = R.drawable.fat_bastard;
				break;
			case R.id.giraffe_2_button:
				drawablePerson = R.drawable.giraffe_2;
				break;
			case R.id.monkey_button:
				drawablePerson = R.drawable.monkey;
				break;
			case R.id.superman_button:
				drawablePerson = R.drawable.superman;
				break;
			default:
				break;	
		}
		return drawablePerson;
	}
	
	@Override
	public void onClick(View v) {
		int selectedButtonId = v.getId();
		int insertPersonResourceId;
		int replaceFaceResourceId;
		//Button ids and their corresponding ImageButtons are matched positionally
		//If the clicked button belongs to the Insert Person set, launch the InsertPersonActivity with the resource id of the image on the bundle
		Log.d(TAG, "id of button pressed: "+selectedButtonId);
		
		if (mPhotoOperation == ADD_PERSON){ //if user chose from insertPerson list of buttons
			Intent insertPerson = new Intent(mMainInstance.getApplicationContext(), InsertPersonActivity.class);
			insertPersonResourceId = getPersonToInsert(selectedButtonId);
			insertPerson.putExtra("DRAWABLE_PERSON_ID", insertPersonResourceId);
			startActivity(insertPerson);
		}
		
		else if (mPhotoOperation == REPLACE_FACE){ //if user chose from replaceFace list of buttons
			Intent replaceFace = new Intent(mMainInstance.getApplicationContext(), ReplaceFaceActivity.class);
			replaceFaceResourceId = getReplacementFace(selectedButtonId);
			replaceFace.putExtra("FACE_ID", replaceFaceResourceId);
			startActivity(replaceFace);
		}
		
	}
}