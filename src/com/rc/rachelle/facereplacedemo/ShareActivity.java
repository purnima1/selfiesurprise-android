package com.rc.rachelle.facereplacedemo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Context;
import android.content.ClipboardManager;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.net.Uri;
import android.telephony.SmsManager;
import android.provider.MediaStore;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.view.*;
import android.view.View.OnClickListener;
import android.util.Base64;
import android.util.Log;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterException;
import twitter4j.StatusUpdate;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.media.ImageUpload;
import twitter4j.media.ImageUploadFactory;
import twitter4j.media.MediaProvider;

import com.facebook.*;
import com.facebook.model.*;
import com.facebook.widget.*;

public class ShareActivity extends Activity implements OnClickListener{
	
	private static final String TAG = "ShareActivity: ";
	
	private static final String API_KEY = "pgkBVv0fRtosGPXhRCQrqqo3U";
	
	private int[] mButtonIDs = {R.id.facebook_button, R.id.twitter_button, R.id.instagram_button, R.id.flickr_button, R.id.message_button,
		R.id.mail_button, R.id.camera_roll_button, R.id.assign_to_contact_button, R.id.copy_button, R.id.print_button};
	private ImageButton[] mButtons;

	private Bitmap mBitmap; //final bitmap
	private File mFile; //final bitmap as File
	private Uri mImageUri; //final bitmap's URI
	private ImageView mImageView; //final bitmap's imageview
	
	private final static int FACEBOOK = 1;
	
	private static Twitter mTwitter;
	private ConnectionDetector mConnectionDetector;
	
	private SmsManager smsManager;
		
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

		// Add code to print out the key hash
		    try {
		        PackageInfo info = getPackageManager().getPackageInfo(
		                "com.rc.rachelle.facereplacedemo", 
		                PackageManager.GET_SIGNATURES);
		        for (Signature signature : info.signatures) {
		            MessageDigest md = MessageDigest.getInstance("SHA");
		            md.update(signature.toByteArray());
		            Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
		            }
		    } catch (NameNotFoundException e) {
				e.printStackTrace();
		    } catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
		    }

		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.share_activity);
		
		smsManager = SmsManager.getDefault();
		
		mBitmap = SelfieSurpriseMainActivity.getBackgroundBMP();
		saveBitmapToFile(mBitmap);
		
		mImageView = (ImageView) findViewById(R.id.saved_picture);
		mImageView.setImageBitmap(mBitmap);
		
		makeImageButtons();
		
	}
	
	private void messageShare(){
		Intent sendIntent = new Intent(Intent.ACTION_VIEW);         
		sendIntent.setData(Uri.parse("sms:"));
		sendIntent.putExtra("sms_body", "Surprise! Check out this twisted Selfie from SelfieSurprise!"); 
		//sendIntent.putExtra(Intent.EXTRA_STREAM, mImageUri);
		//sendIntent.setType("image/jpeg");
		startActivity(sendIntent);
	}
	
	private synchronized void saveBitmapToFile(final Bitmap bitmap) {		
		new AsyncTask<Void, Void, Void>() {
           @Override
           protected Void doInBackground(final Void ... params ) {
         		Log.d(TAG, "Saving bitmap. Size: " + bitmap.getByteCount());
				File file = null;
				File selfieImageLocation = null;
				if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
					selfieImageLocation = new File(android.os.Environment.getExternalStorageDirectory(), "SelfieSurprise/");}
				if (!selfieImageLocation.exists()){ selfieImageLocation.mkdirs();}
				//filenaming: selfieSurprise_before_systemtimemillis
				file = new File(selfieImageLocation, "selfieSurprise_after_"+System.currentTimeMillis()+".bmp");
				Log.d(TAG, "Saving bitmap. Location: " + file.toString());
				try {
					OutputStream os = new FileOutputStream(file);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
					os.flush();
					os.close();
					Log.d(TAG, "Saving bitmap successful");
				} catch (Exception ex) {
					Log.d(TAG, "Saving bitmap failed");
					ex.printStackTrace();
				}
				mFile = file;
                return null;
           }

           @Override
           protected void onPostExecute(final Void result ) {
				Log.d(TAG, "doInBackground done saving bmp");
				Log.d(TAG, "preparing URI from File...");
				mImageUri = Utils.getImageContentUri(getApplicationContext(), mFile); 
				Log.d(TAG, "...DONE.");
           }
       }.execute();
	}
	
	@Override
	public void onBackPressed(){
		//block use of back button
		finish();
	}
	
	private void copyImageToClipBoard(){
		ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newRawUri("SelfieSurpriseImage", mImageUri);
		clipboard.setPrimaryClip(clip); 
		Toast.makeText(getApplicationContext(), "Image copied to clipboard.", Toast.LENGTH_LONG).show();
	}
	
	private void printImage(){
		Toast.makeText(getApplicationContext(), "Searching for printers...", Toast.LENGTH_LONG).show();
		Toast.makeText(getApplicationContext(), "...no printers could be connected.", Toast.LENGTH_LONG).show();
	}
	
	private void makeImageButtons(){
		mButtons = new ImageButton[mButtonIDs.length];
		for (int i = 0; i<mButtons.length; i++){
			mButtons[i] = (ImageButton) findViewById(mButtonIDs[i]);
			mButtons[i].setOnClickListener(this);
		}
	}
	
	private void emailShare(){
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
		intent.putExtra(Intent.EXTRA_SUBJECT, "subject here");
		intent.putExtra(Intent.EXTRA_TEXT, "body text");
		intent.putExtra(Intent.EXTRA_STREAM, mImageUri);
		startActivity(Intent.createChooser(intent, "Send email"));
	}
	
	private void instagramShare(){
		//TODO: THIS ASSUMES THAT THE USER HAS INSTAGRAM INSTALLED AS AN APP ON THEIR DEVICE. WILL CHECK FOR THE APP AVAILABILITY LATER
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		shareIntent.setType("image/*"); // set mime type 
		shareIntent.putExtra(Intent.EXTRA_STREAM, mImageUri); // set uri 
		shareIntent.setPackage("com.instagram.android");
		startActivity(shareIntent);
	}
	
	private void twitterShare(){
		try{
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true)
			  .setMediaProviderAPIKey("APIKEY")
			  .setOAuthConsumerKey("APIKEY")
			  .setOAuthConsumerSecret("APISECRET")
			  .setOAuthAccessToken("USER-OAUTH")
			  .setOAuthAccessTokenSecret("USER-OUAUTHSECRET");
				//TODO: MUST GET THE USER'S ACCESS TOKEN AND TOKEN SECRET; THE ONES ABOVE ARE FOR MY OWN PERSONAL TWITTER ACCOUNT
			TwitterFactory factory = new TwitterFactory(cb.build());
	        Twitter twitter = factory.getInstance();
			String tweetMessage = "Check out my latest creation with Selfie Surprise!";
			
			StatusUpdate status = new StatusUpdate(tweetMessage);
	        status.setMedia(mFile);
	        twitter.updateStatus(status);
	
			//TODO: ADD A TOAST HERE TO NOTIFY USER THAT THEIR TWEET WAS POSTED SUCCESSFULLY
		} catch (TwitterException te) {
            te.printStackTrace();
            Log.d(TAG, "Failed to upload the image: " + te.getMessage());
        }
	}
	
	private void facebookShare(){
	  // start Facebook Login
	  Session.openActiveSession(this, true, new Session.StatusCallback() {

	    // callback when session changes state
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
			if (session.isOpened()) {
			 	// make request to the /me API
				Request.newMeRequest(session, new Request.GraphUserCallback() {

				  // callback after Graph API response with user object
				  @Override
				  public void onCompleted(GraphUser user, Response response) {
						
				  }
				}).executeAsync();
			}
	    }
	  });
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  super.onActivityResult(requestCode, resultCode, data);
	  if (requestCode == FACEBOOK){
	  	Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
	  }
	}

	@Override
	public void onClick(View v) {
		int selectedButtonId = v.getId();
		switch (selectedButtonId){
			case R.id.facebook_button:
				facebookShare();
				break;
			case R.id.twitter_button:
				twitterShare();
				break;
			case R.id.instagram_button:
				instagramShare();
				break;
			case R.id.flickr_button:
				break;
			case R.id.message_button:
				messageShare();
				break;
			case R.id.mail_button:
				emailShare();
				break;
			case R.id.camera_roll_button:
				break;
			case R.id.assign_to_contact_button:
				break;
			case R.id.copy_button:
				copyImageToClipBoard();
				break;
			case R.id.print_button:
				printImage();
				break;
		}
	}
	
	private boolean checkIfSocialAppExists(String packageName){
		return true;
	}
	
}