package com.rc.rachelle.facereplacedemo;

import java.util.List;
import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.GridView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.provider.MediaStore;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView.OnScrollListener;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.util.Log;

public class ChoosePhotoFragment extends Fragment{
	
	private static final String TAG = "ChoosePhotoFragment: ";
	
	private static SelfieSurpriseMainActivity mMainInstance;
	private List<Bitmap> mLibraryPictures;
	private Bitmap mSelectedPicture;
	private ChoosePictureOverlayAdapter mLibraryAdapter;
	private GridView mPhotoLibraryGridView;
	private View mView;
	private int libImgWidth;
	private int libImgHeight;
	private FragmentManager mFragmentManager;
	
	public ChoosePhotoFragment(){}

	public static ChoosePhotoFragment getInstance(SelfieSurpriseMainActivity mainInstance){
		//Take an argument of the SelfieSurpriseMainActivity; it's the only Activity that will ever be calling for this Fragment
		ChoosePhotoFragment choosePhoto = new ChoosePhotoFragment();
		mMainInstance = mainInstance;
		return choosePhoto;
	}
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		super.onCreateView(inflater, container, savedInstanceState);
		Log.d(TAG, "reached onCreateView of the fragment");
        mView = inflater.inflate(R.layout.choose_photo_fragment, container, false);
		Log.d(TAG, "view inflated");
		mPhotoLibraryGridView = (GridView) mView.findViewById(R.id.library_gridview);
		
		libImgWidth = 100;
		libImgHeight = 100;

		mLibraryAdapter = new ChoosePictureOverlayAdapter(mMainInstance, mLibraryPictures, libImgWidth, libImgHeight);
		mPhotoLibraryGridView.setAdapter(mLibraryAdapter);
		Log.d(TAG, "adapter set");
		
		mPhotoLibraryGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				mSelectedPicture = mLibraryPictures.get(position);
				mMainInstance.removePicChooseFragmentPicSelected();
	        }
	    });
	
	/*
		// ONSCROLLLISTENER
		mPhotoLibraryGridView.setOnScrollListener(new OnScrollListener() {
		    @Override
		    public void onScrollStateChanged(AbsListView view, int scrollState) {}

		    @Override
		    public void onScroll(AbsListView view, int firstVisibleItem,
		            int visibleItemCount, int totalItemCount) {
		        int lastInScreen = firstVisibleItem + visibleItemCount;
		
				Log.d(TAG, "lastInScreen: "+lastInScreen);
				Log.d(TAG, "totalItemCount: "+totalItemCount);
				Log.d(TAG, "numberOfPictures: "+numberOfPictures);
		
		        if (lastInScreen == totalItemCount) {
		                // FETCH THE NEXT BATCH OF FEEDS
						Log.d(TAG, "Fetched more pictures with numberOfPictures = "+numberOfPictures);
		                new LoadSDCardImages().execute();
						mLibraryAdapter.notifyDataSetChanged();
						mPhotoLibraryGridView.setAdapter(mLibraryAdapter);
		        }
		    }
		});
		*/
		return mView;
	}
	
	public void instantiateList(){
		mLibraryPictures = new ArrayList<Bitmap>();
	}
	
	public void getTwentyFourPictures(){
		if (mLibraryPictures.size() == 0){
			new LoadSDCardImages().execute();
		}
	}
	
	public Bitmap getSelectedPicture(){
		return mSelectedPicture;
	}
	
	//Async task to load complete list of pictures from the SD card
	private class LoadSDCardImages extends AsyncTask<Void, Void, Integer> {
		
		private static final String TAG = "LoadSDCardImage: ";

	    protected Integer doInBackground(Void...arg0) {
			Log.d(TAG, "Reached background task to fill images");
			
			int columnIndex;
			Cursor cursor;
			int numberOfPictures = 0;
		
			//thanks to http://mihaifonoage.blogspot.in/2009/09/displaying-images-from-sd-card-in.html
			// Get the data location of the image
            String[] projection = {MediaStore.Images.Media.DATA};

			for (String string : projection){
				Log.d(TAG, "In projection: "+string);
			}

            cursor = mMainInstance.getApplicationContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    projection, // Which columns to return
                    null,       // Return all rows
                    null,
                    null);
			
            columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			Bitmap libraryPicture;
		
			//If cursor not empty
			if (cursor.moveToFirst()){
				Log.d(TAG, "Cursor was not null");
                //cursor.moveToPosition(position);
				do{
	                // Get image filename
	                String imagePath = cursor.getString(columnIndex);
					libraryPicture = Utils.decodeSampledBitmapFromUri(imagePath, 100, 100);

					//Allowed null bitmaps (happens sometimes) to pass through Utils.decodeSampled;
					//need to just display those as black squares if they were null
					if (libraryPicture == null){
						libraryPicture = BitmapFactory.decodeResource(mMainInstance.getResources(), R.drawable.black_square);
					}
					Log.d(TAG, "Found "+numberOfPictures+"th image at: "+imagePath);
					mLibraryPictures.add(libraryPicture);
					numberOfPictures++;
				} while (cursor.moveToNext() && numberOfPictures < 16);	
			}
			Log.d(TAG, "Total number of pictures: "+numberOfPictures);
			return numberOfPictures;
	    }

	    protected void onProgressUpdate(int numberOfPictures) {}
	}
	
	
}