package com.rc.rachelle.facereplacedemo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Files.FileColumns;
import android.net.Uri;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.*;
import android.view.ViewGroup.LayoutParams;
import android.util.AttributeSet;
import android.util.Log;



public class TakePictureActivity extends Activity implements View.OnTouchListener 
{
	
	  private static final String TAG = "TakePictureActivity";
	  private static final int REQUEST_CODE = 1;
	  private static final int TAKE_PICTURE = 2;
	  private static final int MEDIA_TYPE_IMAGE = 1;
	  private static final int MAX_FACES = 5;
	  private Bitmap mBitmap;
	  private ImageView mImageView;
	  private Uri capturedImageURI;
	  private RectF[] faceFrames;
	  private LinearLayout mLinearLayout;
	  private int touchCount; //after second touch event, we know we've marked both eye positions
	  private int eye1_x, eye1_y, eye2_x, eye2_y;
	  private int last_x, last_y = 0;
      private double eyeDistance;
	  private float mEyeDistance;
	  private DrawOnBMP mDrawOnBMP;
	  private BallView mBallView;
	  private RelativeLayout mRelativeLayout;
	  private Button mMakeMask;
	  private int x, y;
	  private float newWidth = 100f;
	  private float newHeight = 100f;
	  private double facialAngle;
	  private float faceLength;
	
	  //Toast stuff
	  Context context;
	  int duration;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_picture_activity);

		mLinearLayout = (LinearLayout) findViewById(R.id.root_layout);
		//mImageView = (ImageView) findViewById(R.id.facedet);
		
		context = getApplicationContext();
		duration = Toast.LENGTH_LONG;
		
		touchCount = 0;
		
		mDrawOnBMP = new DrawOnBMP(this);
		
		//Todo: change these to the width and height of the underlying bitmap
		int w = getWindowManager().getDefaultDisplay().getWidth()-25;
	    int h = getWindowManager().getDefaultDisplay().getHeight()-25;
		mBallView = new BallView(this, w, h);
		
		mRelativeLayout = (RelativeLayout) findViewById(R.id.drawingView);
		
		
		/*
		mMakeMask = new Button(this);
		mMakeMask.setText("Use this");
		mMakeMask.setLayoutParams(new LayoutParams(
	        ViewGroup.LayoutParams.WRAP_CONTENT,
	            ViewGroup.LayoutParams.WRAP_CONTENT));
		mMakeMask.setOnClickListener(new Button.OnClickListener() {
			    @Override
		        public void onClick(View v)
		            {
		                //mDrawOnBMP.prepare
		();
						//mDrawOnBMP.invalidate();
						mDrawOnBMP.erase();
						mDrawOnBMP.invalidate();
						mBallView.prepareFaceMask();
						mRelativeLayout.addView(mBallView);
		            }
		         });
		*/
		
		if (mBitmap == null){ //If we've not already taken a picture
			Log.i(TAG, "mBitmap was null on this load of TakePictureActivity");
			Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	   		File imagesFolder = new File(Environment.getExternalStorageDirectory(), "FaceReplaceDemoPics");
	        imagesFolder.mkdirs(); 
	        File image = new File(imagesFolder, "FaceReplacement.bmp");
	        capturedImageURI = Uri.fromFile(image);
	        //imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageURI);
	        startActivityForResult(imageIntent, TAKE_PICTURE);
		}
    }
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK){
			if (data != null){
				mBitmap = (Bitmap) data.getExtras().get("data"); //passing the bitmap back in on the extras bundle results in crap quality
				//mBitmap = BitmapFactory.decodeFile(getRealPathFromURI(capturedImageURI));
				//mBitmap = Utils.RotateBitmap(mBitmap, 270);
				mBitmap = Utils.convert(mBitmap, Bitmap.Config.RGB_565); //convert to RGB565 for face tools to work
				//mImageView.setImageBitmap(mBitmap);
				mImageView = new ImageView(this);
				mImageView.setImageBitmap(mBitmap);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				  LinearLayout.LayoutParams.MATCH_PARENT, 
				  LinearLayout.LayoutParams.MATCH_PARENT);
				mImageView.setLayoutParams(lp);
				mRelativeLayout.addView(mImageView);
				findFaces();
			}
			
		}
	}
	
	private boolean findFaces(){
	//Use the Android Face detector to determine if there even is a face in the picture
		FaceDetector face_detector = new FaceDetector(mBitmap.getWidth(), mBitmap.getHeight(), MAX_FACES);
		FaceDetector.Face[] faces = new FaceDetector.Face[MAX_FACES];
	    int face_count = face_detector.findFaces(mBitmap, faces);
		if (face_count == 0){ 
			Log.i(TAG, "0 faces found");
			CharSequence text = "No faces found in the picture -- please take or choose another!";
			Toast toast = Toast.makeText(context, text, duration);
			toast.show();
			mImageView.setImageBitmap(mBitmap);
			return false;
		} 
		else if (face_count >= 1){
			Log.i(TAG, ">=1 face found");
			prepareFrames(faces);
			//drawRectangles();
			mBallView.prepareFaceMask();
			mRelativeLayout.addView(mBallView);
			return true; 
		}
		return false;
	}
	
	private void drawRectangles(){
		//Bitmap tempBitmap = Bitmap.createScaledBitmap(mBitmap, mBitmap.getWidth(), mBitmap.getHeight(), true);
		Canvas canvas = new Canvas(mBitmap);
		Paint paint = new Paint();
		paint.setStrokeWidth(2);
		paint.setColor(Color.BLUE);
		paint.setStyle(Style.STROKE);
		
		for (int i=0; i < faceFrames.length; i++) {
			RectF r = faceFrames[i];
			if (r != null){
				canvas.drawRect(r, paint);
				Log.d(TAG, "Drew rectangle");
			}
		}
		mImageView.setImageResource(0);
		mImageView.setImageBitmap(mBitmap);
		mImageView.draw(canvas);	
	}
	
	private void prepareFrames(FaceDetector.Face[] facesFound){
		faceFrames = new RectF[facesFound.length];
		Log.d("FaceDet", "Faces Found: "+facesFound.length);
		for (int i=0; i < facesFound.length; i++) {
		    Face face = facesFound[i];
		    Log.d("FaceDet", "Face ["+face+"]");
		    if (face != null) {
		        Log.d("FaceDet", "Face ["+i+"] - Confidence ["+face.confidence()+"]");
		        PointF pf = new PointF();
		        face.getMidPoint(pf);
		        Log.d("FaceDet", "\t Eyes distance ["+face.eyesDistance()+"] - Face midpoint ["+pf+"]");
		        RectF r = new RectF();
		        r.left = pf.x - face.eyesDistance(); //removed /2 from all, but the right and left/2 make for good eye positioning points
		        r.right = pf.x + face.eyesDistance();
		        r.top = pf.y - face.eyesDistance();
		        r.bottom = pf.y + face.eyesDistance();
		        faceFrames[i] = r;
		    }
		 	mEyeDistance = facesFound[0].eyesDistance();
		}
	}
	
	private String getRealPathFromURI(Uri contentURI) {
	    String result;
	    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		Log.d(TAG,"Content URI: "+contentURI);
	    if (cursor == null) { // Source is Dropbox or other similar local file path
	        result = contentURI.getPath();
			Log.d(TAG,"Image URI: "+result);
	    } else { 
	        cursor.moveToFirst(); 
	        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
	        result = cursor.getString(idx);
			Log.d(TAG,"Image URI: "+result);
	        cursor.close();
	    }
	    return result;
	}
	
	@Override
	    public boolean onTouch(View v, MotionEvent event) {
	       return true;
	    }


	@Override
	    public boolean onTouchEvent(MotionEvent event) {
			//Width of user's fingertip pressing means multiple touch events registered within very short proximity -- should make sure points are 
			//at least 15 pixels apart, so that we're only paying attention to the touch events that signify marking of the eye points			
			x = (int) event.getX();
			y = (int) event.getY();
			Log.i(TAG, "Touch event registered at "+x+", "+y);

			if ((last_x == 0) && (last_y == 0) && touchCount == 0){ //if this is our first touch on the screen, set last_x and last_y to x and y
				last_x = x;
				last_y = y;
				touchCount = 1;
				mDrawOnBMP.setEyePosition(x, y, 1);
				//mRelativeLayout.addView(mDrawOnBMP);
			}

			if ((Math.abs(last_x - x) >= 5) && (Math.abs(last_y - y) >= 5) && touchCount == 1){ //if x difference is greater than 15
				Log.i(TAG, "Second eye placed");
				touchCount = 2;
				last_x = x;
				last_y = y;
				runOnUiThread(new Runnable() {
				    public void run() {
						mDrawOnBMP.setEyePosition(x, y, 2);
						mDrawOnBMP.invalidate();
				    }
				});
			}
			
			if ((Math.abs(last_x - x) >= 5) && (Math.abs(last_y - y) >= 5) && touchCount == 2){ //if x difference is greater than 15
				Log.i(TAG, "Top-Center hairline placed");
				touchCount = 3;
				last_x = x;
				last_y = y;
				runOnUiThread(new Runnable() {
				    public void run() {
						mDrawOnBMP.setCenterLine(x, y, 1);
						mDrawOnBMP.invalidate();
				    }
				});
			}
			
			if ((Math.abs(last_x - x) >= 5) && (Math.abs(last_y - y) >= 5) && touchCount == 3){ //if x difference is greater than 15
				Log.i(TAG, "Chin position placed");
				touchCount = 4;
				last_x = x;
				last_y = y;
				runOnUiThread(new Runnable() {
				    public void run() {
						mDrawOnBMP.setCenterLine(x, y, 2);
						mDrawOnBMP.invalidate();
						mRelativeLayout.addView(mMakeMask);
				    }
				});
			}

	        return true;
	   }
	
	private class DrawOnBMP extends View{
			String TAG = "DrawOnBMP";

			DrawOnBMP instance;
			Paint eyePaint, centerlinePaint;
		 	int eye1_x, eye1_y, eye2_x, eye2_y;
			int centerHairlineX, centerHairlineY, centerChinX, centerChinY;
			int mLayerNumber = 0;
			Bitmap faceMask;

			public DrawOnBMP(Context context) {
				super(context);
				instance = this;
			}

			public DrawOnBMP(Context context, AttributeSet attrs)
					throws IOException {
				super(context, attrs);
				instance = this;
			}

			public DrawOnBMP(Context context, AttributeSet attrs, int defStyle)
					throws IOException {
				super(context, attrs, defStyle);
				instance = this;
			}
			
			private void init(){
				setWillNotDraw(false);
			}
			
			public void setEyePosition(int x, int y, int eyeNumber){
				if (eyeNumber == 1){
					eye1_x = x;
					eye1_y = y;
					mLayerNumber = 1; //if 1, we're on the first layer. if 2, we're on the second.
				}
				else if (eyeNumber == 2){
					eye2_x = x;
					eye2_y = y;
					mLayerNumber = 2;
				}
			}
			
			public void setCenterLine(int x, int y, int topOrBottom){
				if (topOrBottom == 1){ //point is the mid-center hairline
					centerHairlineX = x;
					centerHairlineY = y;
					mLayerNumber = 3;
				}
				if (topOrBottom == 2){
					centerChinX = x;
					centerChinY = y;
					mLayerNumber = 4;
				}
			}
			
			private void calculateFaceLength(){ //these two can obvi be merged together and args given as parameters, just doing it all sloppy-like for now
			 	double xDist = Math.pow((centerChinX-centerHairlineX), 2);
				double yDist = Math.pow((centerChinY-centerHairlineY), 2);
				faceLength = (float) Math.sqrt(xDist+yDist);
				Log.i(TAG, "Hairline to chin distance = "+faceLength);
			}
			
			private void calculateEyeDistance(){
			 	double xDist = Math.pow((eye2_x-eye1_x), 2);
				double yDist = Math.pow((eye2_y-eye1_y), 2);
				eyeDistance = Math.sqrt(xDist+yDist);
				Log.i(TAG, "eyeDistance = "+eyeDistance);
			}
			
			private void calculateFacialAngle(){
				int yDiff = centerHairlineY-centerChinY;
				//angle can be found as the cosecant, which = hypoteneuse/opposite; or the faceLength/yDiff
				double cosecantTheta = 1/Math.sin(faceLength/yDiff);
				facialAngle = Math.abs(Math.pow(cosecantTheta, -1));
				facialAngle = Math.toDegrees(facialAngle);
				Log.i(TAG, "facialAngle = "+facialAngle);
			}
			
			public void erase(){
				mLayerNumber = 5; 
			}
			
			@Override
			protected void onDraw(Canvas canvas) {
				super.onDraw(canvas);
				
				Log.i(TAG, "onDraw called");
				
				eyePaint = new Paint();
				eyePaint.setStrokeWidth(5);
				eyePaint.setColor(Color.WHITE);
				eyePaint.setStyle(Style.STROKE);
				
				centerlinePaint = new Paint();
				centerlinePaint.setStrokeWidth(4);
				centerlinePaint.setColor(Color.BLUE);
				centerlinePaint.setStyle(Style.STROKE);

				if (mLayerNumber == 1){
					canvas.drawCircle(eye1_x, eye1_y-20, 5, eyePaint); //y positions were little screwy on the tab2
				}
				else if (mLayerNumber == 2){
					canvas.drawCircle(eye1_x, eye1_y-20, 5, eyePaint);
					canvas.drawCircle(eye2_x, eye2_y-20, 5, eyePaint);
					calculateEyeDistance();
				}
			    else if (mLayerNumber == 3){
					canvas.drawCircle(eye1_x, eye1_y-20, 5, eyePaint);
					canvas.drawCircle(eye2_x, eye2_y-20, 5, eyePaint);
					canvas.drawCircle(centerHairlineX, centerHairlineY-20, 4, centerlinePaint);
				}
				else if (mLayerNumber == 4){
					canvas.drawCircle(eye1_x, eye1_y-20, 5, eyePaint);
					canvas.drawCircle(eye2_x, eye2_y-20, 5, eyePaint);
					canvas.drawCircle(centerHairlineX, centerHairlineY-20, 4, centerlinePaint);
					canvas.drawCircle(centerChinX, centerChinY-20, 4, centerlinePaint);
					calculateFaceLength();
					calculateFacialAngle();
				}
				else if (mLayerNumber == 5){
					//Do nothing, effectively erasing the canvas
				}
			}
		}
	
	public class BallView extends SurfaceView implements SurfaceHolder.Callback {
		private static final String TAG = "BallView";
	    private Bitmap faceMask;
	    private int x=20,y=20;int width,height;

	    public BallView(Context context,int w,int h) {
	        super(context);

	        width=w;
	        height=h;
	        getHolder().addCallback(this);
	        setFocusable(true);
	
			faceMask = BitmapFactory.decodeResource(getResources(), R.drawable.johnsnewbraddface);
	
			this.setZOrderOnTop(true);    // necessary
			SurfaceHolder sfhTrackHolder = this.getHolder();
			sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);
	    }

	    @Override
	    protected void onDraw(Canvas canvas) {
	        super.onDraw(canvas);
	        canvas.drawBitmap(faceMask,x-(faceMask.getWidth()/2),y-(faceMask.getHeight()/2),null);
	    }

	    @Override
	    public boolean onTouchEvent(MotionEvent event) {
	        x=(int)event.getX();
	        y=(int)event.getY();

	        if(x<25)
	            x=25;
	        if(x> width)   
	            x=width;
	        if(y <25)
	            y=25;
	        if(y > height)
	            y=height;

	        updateBall();

	        return true;
	    }
	
		public void prepareFaceMask(){
			Log.i(TAG, "Reached mask prep code");
			
			//John's new Brad pitt mask is 106 px in eye distance
			float scaleFactor = (float) 106 / mEyeDistance;
			float newWidth = faceMask.getWidth() * scaleFactor;
			
			//Queen mask; eye distance = 84, brad pitt is 77
			
			//float scaleFactorY = (float) faceLength / 235; //brad pitt mask is 235 px long
			float newHeight = faceMask.getHeight() * scaleFactor;
			Log.i(TAG, "Scaled faceMask to size: "+newWidth+", "+newHeight);
	
			//faceMask = Utils.scaleBitmap(faceMask, newWidth, newHeight);
			//faceMask = Utils.rotateBitmap(faceMask, 7); //rotate 7 deg
		}

	    @Override
	    public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
	        // TODO Auto-generated method stub
	    }

	    @Override
	    public void surfaceCreated(SurfaceHolder holder) {
	        // TODO Auto-generated method stub
	    }

	    @Override
	    public void surfaceDestroyed(SurfaceHolder holder) {
	        // TODO Auto-generated method stub
	    }

	    private void updateBall() {
	        Canvas canvas = null;
	        try {
	            canvas = getHolder().lockCanvas(null);
	            synchronized (getHolder()) {
					canvas.drawColor( 0, PorterDuff.Mode.CLEAR );
	                this.onDraw(canvas);
	            }
	        }
	        finally {
	            if (canvas != null) {
	                getHolder().unlockCanvasAndPost(canvas);
	            }
	        }
	    }   
	}

}