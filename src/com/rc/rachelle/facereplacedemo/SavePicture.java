package com.rc.rachelle.facereplacedemo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.view.*;
import android.util.Log;

public class SavePicture extends Activity{
	
	private static final String TAG = "SavePicture: ";
	
	private Bitmap mBitmap;
	private Uri mBitmapUri;
	
	private ImageButton mApprove, mDiscard;

	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.save_picture_activity);	
		
		Intent intent = getIntent();
		mBitmapUri = Uri.parse(intent.getStringExtra("bmp"));
		getImage();
		
		Log.d(TAG, "URI: "+mBitmapUri.toString());

		ImageView imageView = (ImageView) findViewById(R.id.saved_picture);
		
		//button click listeners
		mApprove = (ImageButton) findViewById(R.id.approve_button);
		
		mApprove.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v)
	            {
					Intent share = new Intent(SavePicture.this, ShareActivity.class);
					share.putExtra("uri", mBitmapUri.toString());
					startActivity(share);
	            }
	         });
		
		imageView.setImageBitmap(mBitmap);
	}
		
	private void getImage(){
		int newHeight, newWidth = 0;

		try{
			mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mBitmapUri);
			//trying to crop the top off, as there's this lingering black bar at the top that i can't get rid of
			mBitmap = Bitmap.createBitmap(mBitmap, 0, 25, mBitmap.getWidth(), mBitmap.getHeight()-25);
			newHeight = (int) Math.round(mBitmap.getHeight()*.8);
			newWidth = (int) Math.round(mBitmap.getWidth()*.8);
			mBitmap = Utils.scaleBitmap(mBitmap, newWidth, newHeight);
			//mBitmap = Utils.getResizedBitmap(mBitmap, newWidth, newHeight);
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		
	}

	
}