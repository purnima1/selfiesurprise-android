package com.rc.rachelle.facereplacedemo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.ImageFormat;
import android.media.AudioManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.Window;

import com.rc.rachelle.facereplacedemo.R;

public class SelfieSurpriseMainActivity extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.ErrorCallback
{
	private static final String TAG = "SelfieSurpriseMainActivity";
	
	private static final String CHOOSE_PHOTO_FRAGMENT = "ChoosePhotoFragment";
	private static final String IMAGE_MANIPULATION_FRAGMENT = "ImageManipulationFragment";
	private static final int TAKE_PHOTO = 1;
	private static final String IMAGE_URI = "imageUri";
	private static final int FRONT_CAMERA = Camera.CameraInfo.CAMERA_FACING_FRONT;
	private static final int BACK_CAMERA = Camera.CameraInfo.CAMERA_FACING_BACK;
	private static final int FLASH_OFF = 0;
	private static final int FLASH_AUTO = 1;
	private static final int FLASH_ON = 2;
	
	private int mCameraDirection = FRONT_CAMERA;
	private int mCameraFlash = FLASH_AUTO;
	
	private static List<Bitmap> mDrawingStack; //this is used by the image manipulation 
	
	private Camera mCamera;

	private ImageButton mCameraDirectionButton;
	private ImageButton mCameraFlashButton;
	private ImageButton mChoosePictureSlider;
	private ImageButton mTakePictureButton;
	private ImageView mSplashView;
	private RelativeLayout mTaskBar; //top taskbar
	private FrameLayout mBottomFragmentContainer;
	private RelativeLayout mMiddleContent; //all the image stuff between the two taskbars
	private LinearLayout mRootLinearLayout;
	
	private ChoosePhotoFragment mChoosePhotoFragment;
	private ImageManipulationFragment mImageManipulationFragment;

	private static Bitmap mMainImage; //blank background set to pic that user takes/chooses

	private List<Bitmap> mLibraryPictures;
	public boolean mSliderOut; //this public so it can be toggled from fragments
	
	private FragmentManager mFragmentManager;
 	private FragmentTransaction mFragmentTransaction;

    private Resources mResources;

	private Uri mSelfieUri;
	
	//private Preview mPreview;
	private SurfaceView mSurfaceView;
	private SurfaceHolder mSurfaceHolder;
	boolean mPreviewRunning, mIsDone = false;
	
	private ShutterCallback mShutterCallback;
	private PictureCallback mPictureCallback;
	
	private Boolean mFirstLoad; //get this from the Intent data
	
	private Camera.Parameters parameters;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

		if (checkForExtras()){
			mFirstLoad = false;
		}
		else{
			mFirstLoad = true;
		}

		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.main);
		
		mResources = getResources();
		
		mSliderOut = false;
		
		mRootLinearLayout = (LinearLayout) findViewById(R.id.root_linearlayout);
		
		mTaskBar = (RelativeLayout) findViewById(R.id.top_toolbar);
		mTakePictureButton = (ImageButton) findViewById(R.id.take_picture_button);

		mBottomFragmentContainer = (FrameLayout) findViewById(R.id.bottom_fragment_container);
		mChoosePictureSlider = (ImageButton) findViewById(R.id.choose_picture_slider);
		mCameraFlashButton = (ImageButton) findViewById(R.id.flash_option);
		mCameraDirectionButton = (ImageButton) findViewById(R.id.toggle_camera);
		
		mMiddleContent = (RelativeLayout) findViewById(R.id.middle_content);
		//mChoosePhotoFragContainer = (LinearLayout) findViewById(R.id.photo_fragment_container);
		
		mSplashView = (ImageView) findViewById(R.id.splash_view);
		
		mFragmentManager = getFragmentManager();
		
		//Click mChoosePictureSlider button, and if it's not already pulled-out, display the GridView. If the GridView is pulled out already,
		//and the user clicks mChoosePictureSlider, we will hide the GridView again.
		mChoosePictureSlider.setOnClickListener(new ImageButton.OnClickListener() {  
		        public void onClick(View v)
		            {
					   if (mSliderOut == false){
						   launchPicChooseFragment();
						   mSliderOut = true;
						}
						else{
						   removePicChooseFragmentPicNotSelected();
						   mSliderOut = false;
						}
		            }
		         });
		
		mCameraFlashButton.setOnClickListener(new ImageButton.OnClickListener() {  
		        public void onClick(View v)
		            {
						mCameraFlash = toggleFlash();
						Log.d(TAG, "Camera flash set to: "+mCameraFlash);
		            }
		         });
		
		mCameraDirectionButton.setOnClickListener(new ImageButton.OnClickListener() {  
		        public void onClick(View v)
		            {
						mCameraDirection = toggleDirection();
						setSurfaceView(false);
						Log.d(TAG, "Camera direction set to: "+mCameraDirection);
		            }
		         });
	
		mTakePictureButton.setOnClickListener(new ImageButton.OnClickListener() {  
		        public void onClick(View v)
		            {
						takeSelfie();
		            }
		         });

		/*
		// Check whether we're recreating a previously destroyed instance
	    if (savedInstanceState != null) {
	        // Restore value of members from saved state
	        mSelfieUri = Uri.parse(savedInstanceState.getString(IMAGE_URI));
			Log.d(TAG, "savedInstanceState not null; it carries: "+mSelfieUri.toString());
			setSelfieImage();
	    }
		*/
		
		if (mChoosePhotoFragment == null){
			mChoosePhotoFragment = ChoosePhotoFragment.getInstance(this);
			mChoosePhotoFragment.instantiateList();
			mChoosePhotoFragment.getTwentyFourPictures();
		}
		
		if (mFirstLoad == true){
			mSplashView.setVisibility(View.VISIBLE);
			mChoosePictureSlider.setVisibility(View.GONE);
			
			//mSplashView: imageview containing splash image visible for 5 seconds;		
			new CountDownTimer(3000,1000){
		        @Override
		        public void onTick(long millisUntilFinished){} 

		        @Override
		        public void onFinish(){
					mainScreenUIVisible();
					setSurfaceView(true);
		        }
		   		}.start();
		}
		else{
			mRootLinearLayout.invalidate();
			mainScreenUIVisible();
			setSurfaceView(true);
		}
    }

	private boolean checkForExtras(){
		// You can be pretty confident that the intent will not be null here.
		Intent intent = getIntent();
		boolean hasExtra = false;
		// Get the extras (if there are any)
		Bundle extras = intent.getExtras();
		if (extras != null) {
		    if (extras.containsKey(Utils.FIRST_LOAD)) {
				hasExtra = true;
		    }
			else{
				 hasExtra = false;
			}
		}
		return hasExtra;
	}

	private void mainScreenUIVisible(){
		mTaskBar.setVisibility(View.VISIBLE);
		mTakePictureButton.setVisibility(View.VISIBLE);
		mSplashView.setVisibility(View.GONE);
	    mChoosePictureSlider.setVisibility(View.VISIBLE);
		mBottomFragmentContainer.setVisibility(View.VISIBLE);
	}

	private void setSurfaceView(boolean firstOpening){
		if (firstOpening){
			mSurfaceView = (SurfaceView) findViewById(R.id.surface);
			mSurfaceView.setVisibility(View.VISIBLE);
			boolean opened = safeCameraOpen(mCameraDirection);
			Log.d(TAG, "Camera opened? -- "+opened);
			mSurfaceHolder = mSurfaceView.getHolder();
			mSurfaceHolder.addCallback(this);
			mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		else{
			Log.d(TAG, "call to reset the view with new camera direction");
			mSurfaceHolder.removeCallback(this);
			mCamera.stopPreview();
			//mSurfaceHolder = null;
			mCamera.release();

			boolean opened = safeCameraOpen(mCameraDirection);
			Log.d(TAG, "Camera opened? -- "+opened);
			mSurfaceHolder = mSurfaceView.getHolder();
			mSurfaceHolder.addCallback(this);
			mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
			try{
				mCamera.setPreviewDisplay(mSurfaceHolder);
			} catch (IOException e){
				e.printStackTrace();
			}
			mSurfaceView.invalidate();
			
			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setPreviewFormat(ImageFormat.NV21);						
			List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
			Camera.Size cs = sizes.get(0);								
			parameters.setPreviewSize(cs.width, cs.height);
			mCamera.setDisplayOrientation(90);

			mCamera.setParameters(parameters);
			
			mCamera.startPreview();
		}
	}
	
	private boolean safeCameraOpen(int id) {
	    boolean qOpened = false;

	    try {
	        releaseCameraAndPreview();
	        mCamera = Camera.open(id);
	        qOpened = (mCamera != null);
	    } catch (Exception e) {
	        Log.e(getString(R.string.app_name), "failed to open Camera");
	        e.printStackTrace();
	    }
	
	    return qOpened;    
	}
	
	private void releaseCameraAndPreview() {
	    //mSurfaceView.setCamera();
	    if (mCamera != null) {
	        mCamera.release();
	        mCamera = null;
	    }
	}

	@Override
    protected void onResume(){
        super.onResume();
	}
	
	@Override
	public void onError(int error, Camera camera) {
		if(error == 100)
			mCamera = Camera.open(mCameraDirection);
	}

	@Override
	public void onPreviewFrame(final byte[] data, final Camera camera) {
		if(mPreviewRunning){
			mCamera.stopPreview();}
			
		mPreviewRunning = false;

		Log.d(TAG, "Preview Frame called. Size: " + data.length);
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated");
		//mCamera = Camera.open(mCameraDirection);
		try{
			mCamera.setPreviewDisplay(holder);
			Log.d(TAG, "mSurfaceHolder passed to camera");
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		Log.d(TAG, "surfaceChanged");
		
		//mCamera = Camera.open(mCameraDirection);
		
		while(mCamera == null){}

		if (mPreviewRunning) {
			mCamera.stopPreview();
		}

		parameters = mCamera.getParameters();
		parameters.setPreviewFormat(ImageFormat.NV21);						
		List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
		Camera.Size cs = sizes.get(0);								
		parameters.setPreviewSize(cs.width, cs.height);
		mCamera.setDisplayOrientation(90);

		mCamera.setParameters(parameters);
		try {
			mCamera.setPreviewDisplay(holder);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mCamera.startPreview();
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.e(TAG, "surfaceDestroyed");
		if (mCamera != null) {
			if(mPreviewRunning)
				mCamera.stopPreview();
			mPreviewRunning = false;
			mCamera.release(); 
			//mCamera = null;
		}
	}

	private int toggleFlash(){
		Drawable button;
		int flash = 0;
		
		if (mCameraFlash == FLASH_OFF){
			//if off, switch to auto
			button = getResources().getDrawable(R.drawable.button_flashauto_default);
			mCameraFlashButton.setBackgroundDrawable(button);
			mCameraFlashButton.invalidate();
			flash = FLASH_AUTO;
		}
		else if (mCameraFlash == FLASH_AUTO){
			//if auto, switch to on
			button = getResources().getDrawable(R.drawable.button_flashon_default);
			mCameraFlashButton.setBackgroundDrawable(button);
			mCameraFlashButton.invalidate();
			flash = FLASH_ON;
		}
		else if (mCameraFlash == FLASH_ON){
			//if on, switch to off
			button = getResources().getDrawable(R.drawable.button_flashoff_default);
			mCameraFlashButton.setBackgroundDrawable(button);
			mCameraFlashButton.invalidate();
			flash = FLASH_OFF;
		}
		
		return flash;
	}
	
	private boolean changeFlashParams(){
		boolean flashSet = false;
		if (mCamera == null){
			if (mCameraFlash == FLASH_OFF){
				parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
				mCamera.setParameters(parameters);
				flashSet = true;
			}
			else if (mCameraFlash == FLASH_AUTO){
				parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
				mCamera.setParameters(parameters);
				flashSet = true;
			}
			else if (mCameraFlash == FLASH_ON){
				parameters.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
				mCamera.setParameters(parameters);
				flashSet = true;
			}
		}
		return flashSet;
	}
	
	private int toggleDirection(){
		int direction = 0;
		
		if (mCameraDirection == FRONT_CAMERA){
			direction = BACK_CAMERA;
		}
		else if (mCameraDirection == BACK_CAMERA){
			direction = FRONT_CAMERA;
		}
		
		return direction;
	}

	private void takeSelfie(){
		
		mShutterCallback = new ShutterCallback() {
		        public void onShutter() {
		            AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		            mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
		        }
		    };
		
		mPictureCallback = new PictureCallback() {
		      // Called for each frame previewed
		      public void onPictureTaken(byte[] data, Camera camera) {
			        Log.d(TAG, "onPictureTaken called at: " + System.currentTimeMillis());
					if (data == null){
						Log.d(TAG, "data came back in null!");
					}
					InputStream is = new ByteArrayInputStream(data);
					
					Bitmap bmp = null;
					
					if (mCameraDirection == BACK_CAMERA){
						BitmapFactory.Options bounds = new BitmapFactory.Options();
						bounds.inSampleSize = 2;
						bmp = BitmapFactory.decodeStream(is, null, bounds);
						
						//This tends to be a problem
						if (bmp.getWidth() > bmp.getHeight()){
							bmp = Utils.rotateBitmap(bmp, 90);
						}
						//bmp = Utils.mirrorBitmap(bmp);
					}
					
					else if (mCameraDirection == FRONT_CAMERA){
						bmp = BitmapFactory.decodeStream(is);
						
						//This tends to be a problem
						if (bmp.getWidth() > bmp.getHeight()){
							bmp = Utils.rotateBitmap(bmp, -90);
						}
						bmp = Utils.mirrorBitmap(bmp);
					}
					
					saveBitmapToFile(bmp);
					mMainImage = bmp;
					initializeDrawingStack();
				
					mCamera.stopPreview();
				
					getRidSurface();
					mMiddleContent.setVisibility(View.INVISIBLE);
				
					BitmapDrawable background = new BitmapDrawable(mMainImage);
					mRootLinearLayout.setBackgroundDrawable(background);
				
					launchManipulationFragment2();
		      }
		    };
		
		mCamera.takePicture(mShutterCallback, null, null, mPictureCallback);
	}
	
	private void getRidSurface(){
		//mSurfaceHolder.removeCallback(this);
		Log.d(TAG, "getRidSurface");
		mSurfaceHolder = null;
		mSurfaceView = null;
	}
	
	private void launchManipulationFragment2(){	
	   //Remove the top toolbar (with the camera-related buttons) from the View
	   mTaskBar.setVisibility(View.GONE);
		
	   //This will launch the fragment to manipulate images, and resize view elements as needed
	   mImageManipulationFragment = ImageManipulationFragment.getInstance(this);
	   mMiddleContent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, getSquashedMiddleContentHeight(), .55f));
	   mBottomFragmentContainer.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, getBottomTaskBarHeight(), .4f));	

	   FragmentTransaction ft2 = mFragmentManager.beginTransaction();
	   ft2.add(R.id.bottom_fragment_container, mImageManipulationFragment, IMAGE_MANIPULATION_FRAGMENT);
	   mBottomFragmentContainer.setVisibility(View.VISIBLE);
	   ft2.addToBackStack(IMAGE_MANIPULATION_FRAGMENT);
	   ft2.commit();
	}
	
	private void killMiddleContent(){
		final Handler handler = new Handler();
		final Runnable r = new Runnable()
		{
		    public void run() 
		    {
		        mMiddleContent.setVisibility(View.INVISIBLE);
				mMiddleContent.postInvalidate();
		        handler.postDelayed(this, 500);
		    }
		};

		handler.postDelayed(r, 500);	
		
	}
	
	//Note: I did a workaround above to check for JELLY_BEAN or below,but this method from Smirk has worked on every device I've tried it on
	//so, I shouldn't need to check for versions again
	private synchronized void saveBitmapToFile(final Bitmap bitmap) {		
		new AsyncTask<Void, Void, Void>() {
           @Override
           protected Void doInBackground(final Void ... params ) {
         		Log.d(TAG, "Saving bitmap. Size: " + bitmap.getByteCount());
				File file = null;
				File selfieImageLocation = null;
				if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
					selfieImageLocation = new File(android.os.Environment.getExternalStorageDirectory(), "SelfieSurprise/");}
				if (!selfieImageLocation.exists()){ selfieImageLocation.mkdirs(); }
				//filenaming: selfieSurprise_before_systemtimemillis
				file = new File(selfieImageLocation, "selfieSurprise_before_"+System.currentTimeMillis()+".bmp");
				Log.d(TAG, "Saving bitmap. Location: " + file.toString());
				try {
					OutputStream os = new FileOutputStream(file);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
					os.flush();
					os.close();
					Log.d(TAG, "Saving bitmap successful");
				} catch (Exception ex) {
					Log.d(TAG, "Saving bitmap failed");
					ex.printStackTrace();
				}
                return null;
           }

           @Override
           protected void onPostExecute(final Void result ) {
				Log.d(TAG, "doInBackground done saving bmp");
           }
       }.execute();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == TAKE_PHOTO && resultCode == Activity.RESULT_OK){
			setSelfieImage();
		}
	}
	
	private void setSelfieImage(){
		try {
			Log.d(TAG, "Selfie URI: "+mSelfieUri);
			//mSelfieUri = Uri.parse("file:///storage/sdcard0/SelfieSurprise/selfiePicture.bmp");
			//Log.d(TAG, "Selfie URI: "+mSelfieUri);
           	mMainImage = MediaStore.Images.Media.getBitmap(getContentResolver(), mSelfieUri);
			if (mMainImage.getWidth() > mMainImage.getHeight()){
				mMainImage = Utils.rotateBitmap(mMainImage, -90.0f);
			}
			selfieChosen();
			launchManipulationFragment(); 
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		if (mSelfieUri != null){
		    savedInstanceState.putString(IMAGE_URI, mSelfieUri.toString());
		    super.onSaveInstanceState(savedInstanceState);
		}
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
	  super.onRestoreInstanceState(savedInstanceState);
	  if (savedInstanceState != null){
	  		mSelfieUri = Uri.parse(savedInstanceState.getString(IMAGE_URI));
	  }
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		if (mCamera != null){
			mCamera.stopPreview();
			mCamera.release();
		}
		finish(); //we want this activity to die quickly
	}
	
	@Override
	protected void onStop(){
		super.onStop();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}
	
	@Override
	protected void onRestart(){
		super.onRestart();
	}
	
	@Override
	protected void onStart(){
		super.onStart();
	}
	
	@Override
	public void onBackPressed(){
		//exit the app if we press back button here
		finish();
	}
	
	private void selfieChosen(){
		mTakePictureButton.setVisibility(View.GONE);
		
		//mMiddleContent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 700, .75f));
	   	mBottomFragmentContainer.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0, 0.0f));

		BitmapDrawable background = new BitmapDrawable(mMainImage);
		mRootLinearLayout.setBackgroundDrawable(background);
		
		mChoosePictureSlider.setVisibility(View.GONE);
		mMiddleContent.invalidate();
	}

	private void launchManipulationFragment(){
	   //Remove the top toolbar (with the camera-related buttons) from the View
	   mTaskBar.setVisibility(View.GONE);	
			
	   //This will launch the fragment to manipulate images, and resize view elements as needed
	   mImageManipulationFragment = ImageManipulationFragment.getInstance(this);
	   mMiddleContent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, getSquashedMiddleContentHeight(), .6f));
	   mBottomFragmentContainer.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, getBottomTaskBarHeight(), .35f));	

	   FragmentTransaction ft2 = mFragmentManager.beginTransaction();
	   ft2.add(R.id.bottom_fragment_container, mImageManipulationFragment, IMAGE_MANIPULATION_FRAGMENT);
	   mBottomFragmentContainer.setVisibility(View.VISIBLE);
	   ft2.addToBackStack(IMAGE_MANIPULATION_FRAGMENT);
	   ft2.commit();
	}

	public void removePicChooseFragmentPicNotSelected(){
	   mTakePictureButton.setVisibility(View.VISIBLE);
	
	   mMiddleContent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 700, .75f));
	   mBottomFragmentContainer.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0, 0.0f));
	
	   mFragmentManager.popBackStack();
	}
	
	public void removePicChooseFragmentPicSelected(){
	   mBottomFragmentContainer.removeAllViewsInLayout();
	   mFragmentManager.popBackStack();
	
	   mCamera.stopPreview();
	   mCamera.release();
	   mCamera = null;
	   mMiddleContent.removeView(mSurfaceView);
	   mMiddleContent.invalidate();
	
	   mMiddleContent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 700, .75f));
	   mBottomFragmentContainer.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0, 0.0f));
		
	   mMainImage = mChoosePhotoFragment.getSelectedPicture();
	   BitmapDrawable background = new BitmapDrawable(mMainImage);
	   initializeDrawingStack();
	   mRootLinearLayout.setBackgroundDrawable(background);
	   mChoosePictureSlider.setVisibility(View.GONE);

	   launchManipulationFragment();
	}
	
	public void launchPicChooseFragment(){
	   mTakePictureButton.setVisibility(View.GONE);
	   mMiddleContent.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, getSquashedMiddleContentHeight(), .7f));
	   mBottomFragmentContainer.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, getBottomTaskBarHeight(), .25f));	
	
	   mFragmentTransaction = mFragmentManager.beginTransaction();
	   mFragmentTransaction.add(R.id.bottom_fragment_container, mChoosePhotoFragment, CHOOSE_PHOTO_FRAGMENT);
	   mFragmentTransaction.addToBackStack(CHOOSE_PHOTO_FRAGMENT);
	   mFragmentTransaction.commit();
	}

	private int getTopTaskbarHeight(){
		//Later will have these dimension getter methods respond to different pixel densities
		//In any event, this stays the same whether or not a fragment is inflated within the view
		int topTaskbarHeight = (int) mResources.getDimension(R.dimen.top_toolbar_height);
		return topTaskbarHeight;
	}
	
	private int getSquashedMiddleContentHeight(){
		//This is for when the choose photo from library fragment is inflated
		int squashedMiddleContent = (int) mResources.getDimension(R.dimen.middle_content_height_after_fragment_addition);
		return squashedMiddleContent;
	}
	
	private int getBottomTaskBarHeight(){
		//Stays the same whether or not a fragment is inflated within the view
		//int bottomTaskbarHeight = 25;
		int bottomTaskbarHeight = (int) mResources.getDimension(R.dimen.image_manipulation_frag_height_compressed);
		return bottomTaskbarHeight;
	}
	
	private int getPhotoLibFragmentHeight(){
		int photoLibFragmentHeight = (int) mResources.getDimension(R.dimen.choose_photo_fragment_height);
		return photoLibFragmentHeight;
	}
	
	private void initializeDrawingStack(){
		if (mDrawingStack == null){
			mDrawingStack = new ArrayList<Bitmap>();
			mDrawingStack.add(mMainImage);
		}
	}
	
	public static Bitmap getBackgroundBMP(){
		//This returns the last element in mDrawingStack, so that multiple drawings can be done on it
		return mDrawingStack.get(mDrawingStack.size()-1);
	}
	
	public static void saveNewDrawing(Bitmap newDrawing){
		Log.d(TAG, "New drawing added to the stack");
		mDrawingStack.add(newDrawing);
	}
	
	public static void removeDrawing(int index){
		mDrawingStack.remove(index);
	}
	
	public static void clearList(){
		mDrawingStack = null;
	}
	
	public class SelfieImage{
		Bitmap drawing;
		int operationType;
		
		public SelfieImage(Bitmap drawing, int operationType){
			this.drawing = drawing;
			this.operationType = operationType;
		}
		
		public int getOperationType(){
			return this.operationType;
		}
	}
}

